/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author David
 */
@ManagedBean (name="log")
@SessionScoped
public class LoginBean implements Serializable {
    private String userName;
    private String userPwd;
    private String userName2="Iniciar Sesion";

    /**
     * Creates a new instance of LoginBean
     */
    public LoginBean() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }
    
    public String login() throws IOException{
        
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext externalcontext = context.getExternalContext();
        HttpServletRequest request = (HttpServletRequest) externalcontext.getRequest();
        HttpSession session = request.getSession();
        String pagina = null;
        
        try{
            System.out.println("usu="+userName);
            System.out.println("upwd="+userPwd);
            request.login(userName, userPwd);
           System.out.println("rol Admin:"+request.isUserInRole("Admin"));
          System.out.println("rol usuario:"+request.isUserInRole("Cliente"));
            if(request.isUserInRole("Admin")){
                System.out.println("buajajajaja");
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("Admin", userName);
                userName2=userName;
            FacesContext.getCurrentInstance().getExternalContext().redirect("/teatro/faces/Admin/inicioAdmin.xhtml");
            
            }
            else{
                if(request.isUserInRole("Cliente")){
                    System.out.println("CHIO!******");
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("Cliente", userName);
                userName2=userName;
                    FacesContext.getCurrentInstance().getExternalContext().redirect("/teatro/faces/Cliente/inicioUsuario.xhtml");
                  //s  pagina = "registro";
                
                }                                       
                else{
                    context.addMessage(null, new FacesMessage("credenciales insuficientes"));
                    pagina = "index";
                }
            }
        } catch(ServletException e){
            //e.printStackTrace();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,"Error en la combinación usuario/contraseña","Error"));
        }
        
        return pagina;
    }
    
    
    public void logout() throws IOException{
        
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        externalContext.invalidateSession();
          HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();   
          HttpSession session = request.getSession();
          userName2="Iniciar sesion";
        try {
            request.logout();
            session.invalidate();
        } catch (ServletException ex) {
            Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        externalContext.redirect(externalContext.getRequestContextPath() + "/faces/index.xhtml");
    }

    public String getUserName2() {
        return userName2;
    }

    public void setUserName2(String userName2) {
        this.userName2 = userName2;
    }

 
    
}
