/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Entidad.Genero;
import Entidad.Obra;
import Sesion.GeneroFacade;
import Sesion.ObraFacade;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;

/**
 *
 * @author Allan
 */
@ManagedBean
@RequestScoped
public class llenarObra {

    private String autor;
    private String titulo;
    private String pais;
    private String director;
    private String actor;
    private int id;
    private int genero;
    
    @Inject
    private ObraFacade llenarObraF;
    private Obra selected = new Obra();
    
     @EJB
     private Sesion.GeneroFacade ejbFacade;
     private List<Genero> generos=null;
    
    private GeneroFacade getFacade() {
        return ejbFacade;
    }
    
    public Obra getSelected() {
        return selected;
    }

    public void setSelected(Obra selected) {
        this.selected = selected;
    }
    
    public List<Genero> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }
    
    
    public List<Obra> getObra(){
        
        
      return this.llenarObraF.findAll();
    }
    
    public int getGenero() {
        return genero;
    }

    public void setGenero(int genero) {
        this.genero = genero;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ObraFacade getLlenarObra() {
        return llenarObraF;
    }

    public void setLlenarObra(ObraFacade llenarObra) {
        this.llenarObraF = llenarObra;
    }
    
    public String guardar(){ 
        Obra o=new Obra();
        o.setIdGenero(selected.getIdGenero());
        o.setTitulo(titulo);
        o.setDirector(director);
        o.setPais(pais);
        o.setActorPrincipal(actor);
        o.setAutor(autor);
        this.llenarObraF.create(o);
        this.id=o.getIdObra();
        return "agregarObra";
    }
    
    public String prepareList() {
        return "agregarObra";
    }
    public String prepareCreate() {
        return "agregarObra";
    }
    
     public String Eliminar(int id)
     {
        Obra o =  this.llenarObraF.find(id);       
        this.llenarObraF.remove(o);
        return "agregarObra";
    }
     
     public String Editar(int id)
    {
        Obra o =  this.llenarObraF.find(id);       
        this.id = o.getIdObra();
        this.genero = o.getIdGenero().getIdGenero();
        this.actor = o.getActorPrincipal();
        this.autor = o.getAutor();
        this.director = o.getDirector();
        this.pais=o.getPais();
        this.titulo=o.getTitulo();
        
        return "editarObra";
    }
     
     public String GuardarEdicion(llenarObra bp, int id)
    {
       Obra o = new Obra();
        o.setIdObra(id);
        o.setIdGenero(selected.getIdGenero());
        o.setTitulo(bp.getTitulo());
        o.setDirector(bp.getDirector());
        o.setPais(bp.getPais());
        o.setActorPrincipal(bp.getActor());
        o.setAutor(bp.getActor());
       this.llenarObraF.edit(o);
       return "agregarObra";
    }
    
    
     
    
    /**
     * Creates a new instance of llenarObra
     */
    public llenarObra() {
    }
    
}
