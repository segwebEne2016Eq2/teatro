/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Entidad.Grupo;
import Entidad.Usuario;
import Sesion.GrupoFacade;
import Sesion.UsuarioFacade;
import static com.sun.faces.facelets.util.Path.context;
import java.io.IOException;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

/**
 *
 * @author David
 */
@ManagedBean
@RequestScoped


public class Crear_Usuario {
private String usuario;
private String a_p;
private String a_m;
private String nombre;
private String telefono;
private String pwd;

private int id_usuario;
private int id_grupo;
@Inject
private UsuarioFacade uf;
@Inject
private GrupoFacade gp;

public String getUsuario() {
        return usuario;
    }

public List<Grupo> getGrupos(){
 return this.gp.findAll();
}

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getA_p() {
        return a_p;
    }

    public void setA_p(String a_p) {
        this.a_p = a_p;
    }

    public String getA_m() {
        return a_m;
    }

    public int getId_grupo() {
        return id_grupo;
    }

    public void setId_grupo(int id_grupo) {
        this.id_grupo = id_grupo;
    }

    public GrupoFacade getGp() {
        return gp;
    }

    public void setGp(GrupoFacade gp) {
        this.gp = gp;
    }

    public void setA_m(String a_m) {
        this.a_m = a_m;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public UsuarioFacade getUf() {
        return uf;
    }

    public void setUf(UsuarioFacade uf) {
        this.uf = uf;
    }


public List<Usuario> getUsuarios(){
    return this.uf.findAll();
}
    /*
     * Creates a new instance of Crear_Usuario
     */
    public Crear_Usuario() {
    }
    
    public void crear() throws IOException{
        Usuario u = new Usuario();
        
        Encriptador en = new Encriptador();
        u.setNombre(nombre);
        u.setApellidoPaterno(a_p);
        u.setApellidoMaterno(a_m);
        u.setUsuario(usuario);
        u.setContrasena(en.Encriptar(pwd));
        u.setTelefono(telefono);       
        this.uf.create(u);
        this.id_usuario=u.getIdUsuario();
        crear_grupo(usuario);
        
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Usuario Registrado"));
        FacesContext.getCurrentInstance().getExternalContext().redirect("/teatro/faces/index.xhtml");    
    }

    public void crear_grupo(String usuario){
        Grupo g= new Grupo();
        g.setRol("Cliente");
        g.setUsuarioGpo(usuario);
        this.gp.create(g);
        this.id_grupo=g.getIdGrupo(); 
    }
}
