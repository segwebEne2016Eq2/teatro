/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;


import Entidad.Funcion;
import Entidad.Obra;
import Entidad.Salas;
import Sesion.FuncionFacade;
import Sesion.ObraFacade;
import Sesion.SalasFacade;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.inject.Inject;
import javax.persistence.Query;

/**
 *
 * @author Allan
 */
@ManagedBean
@RequestScoped
public class llenarFuncion {
    private Date fecha;
    private String horario;
    private int costo;
    private int cupo;
    private int idObra;
    private int idSala;
    private int idFuncion;
    
    
    @Inject
    
    private FuncionFacade llenarFuncionF;
   
    public Date getFecha() {
        return fecha;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }


    public int getCosto() {
        return costo;
    }

    public void setCosto(int costo) {
        this.costo = costo;
    }

    public int getCupo() {
        return cupo;
    }

    public void setCupo(int cupo) {
        this.cupo = cupo;
    }

    public int getIdObra() {
        return idObra;
    }

    public void setIdObra(int idObra) {
        this.idObra = idObra;
    }

    public int getIdSala() {
        return idSala;
    }

    public void setIdSala(int idSala) {
        this.idSala = idSala;
    }

    public int getIdFuncion() {
        return idFuncion;
    }

    public void setIdFuncion(int idFuncion) {
        this.idFuncion = idFuncion;
    }
    
    
    private Funcion selected = new Funcion();
    
    private SalasFacade getFacadeS() {
        return ejbFacadeS;
    }
    @EJB
     private Sesion.SalasFacade ejbFacadeS;
     private List<Salas> salas=null;
     
     public List<Salas> getItemsAvailableSelectOneS() {
        return getFacadeS().findAll();
    }
     
     private ObraFacade getFacadeO() {
        return ejbFacadeO;
    }
    @EJB
     private Sesion.ObraFacade ejbFacadeO;
     private List<Obra> obras=null;
     
     public List<Obra> getItemsAvailableSelectOneO() {
        return getFacadeO().findAll();
    }
      
    
    public Funcion getSelected() {
        return selected;
    }

    public void setSelected(Funcion selected) {
        this.selected = selected;
    }
    
    public List<Funcion> getFuncion(){
      return this.llenarFuncionF.findAll();
       
    }
    
    @EJB
     private Sesion.FuncionFacade ejbFacadeF;
    
    private FuncionFacade getFacadeF() {
        return ejbFacadeF;
    }
    
    public List<Funcion> getItemsAvailableSelectOneF() {
        return getFacadeF().findAll();
    }
     
    
    public void consultaRegistro(Date fecha, String horario, String idSala){
        Funcion obj=new Funcion();
        obj.getFechaFuncion();
        obj.getIdSala();
        obj.getHorario();
        
        
        
    }
    
    public Funcion BuscaFecha(String fecha){
    Funcion funcion=null;
    if(fecha!=null){
        funcion=llenarFuncionF.findByFechaFuncion(fecha);
        if(funcion!=null)
            return funcion;
        
    }
    return funcion;
}
    
    public String guardar(){ 
        Funcion f=new Funcion();
        f.setIdObra(selected.getIdObra());
        f.setIdSala(selected.getIdSala());
        f.setFechaFuncion(fecha);
        f.setHorario(selected.getHorario());
        f.setCosto(costo);
        f.setCupo(selected.getIdSala().getCapacidad());
           
        this.llenarFuncionF.create(f);
        this.idFuncion=f.getIdFuncion();
        return "agregarFuncion";
        
        
    }
    
    
   
    
   

    /**
     * Creates a new instance of llenarFuncion
     */
    public llenarFuncion() {
        DateFormat df =new SimpleDateFormat("dd/MM/yy HH:mm:ss");
        Date dateobj=new Date();
        this.fecha=dateobj;
    }
    
}
