/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sesion;

import Entidad.Funcion;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author David
 */
@Stateless
public class FuncionFacade extends AbstractFacade<Funcion> {
    @PersistenceContext(unitName = "teatroPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FuncionFacade() {
        super(Funcion.class);
    }
    
    public Funcion findByFechaFuncion(String fecha){
        Funcion funcion=null;
        List<Funcion>funciones;
        Query consulta=em.createNamedQuery("Funcion.findByFechaFuncion"); 
        consulta.setParameter("fecha",fecha);      
        funciones=consulta.getResultList();
        if(!funciones.isEmpty()){
            funcion=funciones.get(0);
        }
        return funcion;
    }
    
}
